import controller from 'discourse/controllers/discourse-post-event-builder'
import { inject as controller } from "@ember/controller";
import { action } from "@ember/object";



export default {
  name: "discourse-post-event-builder",
  initialize() {

    controller.reopen({
        composer: controller("composer"),
        @action
        createEvent() {

            if (!this.startsAt) {
            this.send("closeModal");
            return;
            }

            const eventParams = buildParams(
            this.startsAt,
            this.endsAt,
            this.model.eventModel,
            this.siteSettings
            );
            const markdownParams = [];
            Object.keys(eventParams).forEach((key) => {
            let value = eventParams[key];
            markdownParams.push(`${key}="${value}"`);
            });
            this.composer.set('isEvent', true);
            this.toolbarEvent.addText(`[event ${markdownParams.join(" ")}]\n[/event]`);
            this.send("closeModal");
        },
    })
  }
};
