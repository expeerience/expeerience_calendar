# -*- encoding: utf-8 -*-
# stub: rrule 0.4.2 ruby lib

Gem::Specification.new do |s|
  s.name = "rrule".freeze
  s.version = "0.4.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "bug_tracker_uri" => "https://github.com/square/ruby-rrule/issues", "changelog_uri" => "https://github.com/square/ruby-rrule/blob/master/CHANGELOG.md", "homepage" => "https://rubygems.org/gems/rrule", "source_code_uri" => "https://github.com/square/ruby-rrule" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ryan Mitchell".freeze]
  s.date = "2019-02-06"
  s.description = "A gem for expanding dates according to the RRule specification".freeze
  s.email = "rmitchell@squareup.com".freeze
  s.homepage = "https://rubygems.org/gems/rrule".freeze
  s.required_ruby_version = Gem::Requirement.new(">= 2.3.0".freeze)
  s.rubygems_version = "3.2.6".freeze
  s.summary = "RRule expansion".freeze

  s.installed_by_version = "3.2.6" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<activesupport>.freeze, [">= 4.1"])
  else
    s.add_dependency(%q<activesupport>.freeze, [">= 4.1"])
  end
end
